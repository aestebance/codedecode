#INSTALACIÓN Y UTILIZACIÓN
###Construir la imagen y lanzar el contenedor
Dentro de la carpeta del proyecto:

    docker-compose up -d

A continuación en la misma carpeta del proyecto introducir el siguiente comando:

    docker-compose exec -u kiko webserver bash

Ahora, si todo ha ido bien, nos encontramos dentro del contenedor. Debemos instalar las dependencias:

    composer install

Una vez hecho esto ya está todo preparado para trabajar. En la carpeta /src encontraremos el archivo _CodeDecode.php_
que tiene declarados dos métodos públicos _enconde_ y _decode_. Ya sólo queda implementar el código con las instruccciones
que se facilitan a continuación.

Para probar si el código está bien debemos ejecutar el script _test.sh_ de la siguiente forma:

    ./test.sh

Para hacerlo debemos estar en la raíz del proyecto dentro del contenedor (en la carpeta padre de /src).

Una vez ejecutemos el script test.sh éste pasará una serie de pruebas unitarias, y nos devolverá por pantalla si las ha 
pasado todas o no. Ya se mostrará en clase cómo saber si ha pasado todas las pruebas, y si no lo ha hecho cómo saber
cuál ha fallado.

#KATA HAMMING CODE IN PHP

El objetivo de esta KATA es mejorar el pensamiento
algorítmico implementando el "Hamming Code".

##Información de la Kata
El "Hamming Code" es un conocido código que se utiliza para corregir errores,
los conocidos como cambios de bits, en las transmisiones de datos. En esta Kata
vamos a implementar el "Hamming Code" con longitud de bit 3. Obviamente el objetivo de esta kata
no es entender el "Hamming Code" ya que forma parte de la rama de las telecomunicaciones, sino que lo que se pretende
es conseguir implementar su código siguiendo unos pasos que detallaremos a continuación. 

###Requisitos previos
- Para resolver esta kata se deberá tener dominados los siguientes conceptos de programación en PHP:
  
        Tipos de datos (Array, string, char, binary y ASCII). No obstante sólo trabajaremos con Arrays, Strings y chars.
        Recorrer arrays y añadir elementos.
        Convertir cadenas de Strings en arrays y viceversa.
        COnocer el uso de str_split(), join() y array_push()

Cualquier otra función necesaria para desarrollar el "Hamming Code" se facilitará al alumno, aunque deberá ser éste quien 
acuda a la documentación oficial para saber cómo implementarla en el código.


##TAREA 1
Deberemos crear una función denominada _encode_ que reciba un string (de caracteres) como parámero e implementarle las siguientes 
funcionalidades en el orden en el que aparecen (en caso contrario no funcionará):

- Convertir todas las letras del texto en valores ASCII
- Convertir los valores ASCII en binarios de 8-bits.
- Triplicar cada bit.
- Concatenar el resultado.

        Ejemplo:
        input: "hey" --> 104, 101, 121   // LO pasamos a valores ASCII
        --> 01101000, 01100101, 01111001   // Lo pasamos a binario de 8 bits
        --> 000111111000111000000000 000111111000000111000111 000111111111111000000111  // triplicamos cada bit
        Output: --> "000111111000111000000000000111111000000111000111000111111111111000000111"  // concatenamos el resultado

La función debe devolver el resultado concatenado.

HINT: Funciones necesarias: ord(), decbin(), str_pad().
Enel caso de str_pad() pongo a continuación su uso ya que puede resultar dífícil de entender
en la documentación. Lo que hace esta función es concatenar un string con otro hasta una longitud determinada, esto es así
debido a que algunos caracteres tendrán 7 bits y otros 8 bits ya que el propio php nos elimina los 0 de la izquierda
que no son necesarios, sin embargo nosotros necesitamos que todos tengan 8 bits,
por lo tanto aplicando str_pad($binString, 8, 0, STR_PAD_LEFT) le estaremos añadiendo un 0 a la izquierda a todos los
binarios de 7 bits. Por ejemplo, la letra "h" en ASCII es 104, y pasado a binario el resultado es 1101000, si obseravmos
tiene 7 bits, sin embargo necesitamos que tenga 8 para que funcione el "Hammer Code", para ello lo pasamos por la función
str_pad y nos devolverá el siguiente resultado: 01101000. Si por el contrario el binario ya tiene 8 bits str_pad() devolverá
ese mismo binario sin alterarlo.

##TAREA 2
Deberemos crear una función denominada _decode_ que reciba un string (de números binarios) como parámero e implementarle 
las siguientes funcionalidades en el orden en el que aparecen (en caso contrario no funcionará):

Esta función comprueba si se ha producido algún error en el envío y lo corrige. Asumiremos que los errores únicamente
pueden ser un cambio de bit (1 por 0  o 0 por 1) y no la pérdida de ningún bit.

- Dividir la cadena recibida en grupos de tres caracteres.
- Comprobar si ha habido algún error en la transmisión. Si no ha habido errores los tres caracteres de cada grupo deberán
ser el mismo, en caso de que haya alguno cambiado nos quedaremos con el que más se repita.
  
        con errores: 010 -> 0, 101 -> 1 
        sin errores: 000 -> 0, 111 -> 1

- A continuación agruparemos los caracteres resultantes del ejercicio en grupos de 8, el resultado será un array de bytes
  (strings de 8 caracteres en binario).
- Ahora toca convertir los valores binarios a ASCII.
- Por último convertiremos los valores ASCII en caracteres y los concatenaremos para devolver el resultado.

        input: "100111111000111001000010000111111000000111001111000111110110111000010111"
        --> 100, 111, 111, 000, 111, 001, ...  // Dividimos en grupos de tres caracteres
        -->  0,   1,   1,   0,   1,   0,  ...  // Corregimos posibles errores y nos quedamos con el bit que más se repita
        --> 01101000, 01100101, 01111001       // Agrupamos los bits en grupos de 8 para obtener un byte.
        --> 104, 101, 121                      // Transformamos cada grupo de binario a ASCII.
        --> "hey"                              // Concatenamos y devolvemos el reultado obtenido

HINT: Funciones necesarias: substr_count(), bindec(), chr()


Se facilitará un código para realizar pruebas unitarias.